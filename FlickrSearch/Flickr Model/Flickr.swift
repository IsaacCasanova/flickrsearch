//
//  Flickr.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/29/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import Foundation

let apiKey = "bc19d7871ba3214207922bdce4c5db5e"
let apiSecret = "223ebb7daee55fa7"
let perPage = "50"

struct Flickr {
    
    static func buildSearchURLForSearchTerm(_ query: String,
                                            page: String = "1") -> URL? {
        
        guard let encodedSearchTerm = query.addingPercentEncoding(withAllowedCharacters: .alphanumerics) else {
            return nil
        }
        
        let searchURLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(encodedSearchTerm)&page=\(page)&per_page=\(perPage)&format=json&nojsoncallback=1"
        
        guard let url = URL(string: searchURLString) else {
            return nil
        }
        
        return url
    }
}

