//
//  FlickrDataStore.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/30/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import Foundation

private typealias C = Constants

private struct Constants {
    static let storageKey = "FlickrPhotoStorageKey"
}

class FlickrDataStore {
    
    static let shared: FlickrDataStore = {
       return FlickrDataStore()
    }()
    
    private let dispatchQueue = DispatchQueue(label: "FlickrDataStoreQueue",
                                              attributes: .concurrent)
    
    private init() { }
    
    func readPhotos() -> [FlickrPhoto]? {
        
        var photos: [FlickrPhoto]? = nil
        
        dispatchQueue.sync {
            photos = readPhotosHelper()
        }
        
        return photos
    }
    
    func persistPhotos(_ photos: [FlickrPhoto]) {
        
        dispatchQueue.async(flags: .barrier) { [unowned self] in
            self.writePhotos(photos)
        }
    }
}

extension FlickrDataStore {
    
    private func readPhotosHelper() -> [FlickrPhoto]? {
        
        guard let jsonString = UserDefaults.standard.string(forKey: C.storageKey),
            let jsonData = jsonString.data(using: .utf8) else {
                return nil
        }
    
        let jsonDecoder = JSONDecoder()
        
        do {
            return try jsonDecoder.decode([FlickrPhoto].self, from: jsonData)
        } catch {
            print("Error decoding photos: error => \(error)")
            return nil
        }
    }
    
    private func writePhotos(_ photos: [FlickrPhoto]) {
        
        let jsonEncoder = JSONEncoder()
        
        do {
            let jsonData = try jsonEncoder.encode(photos)
            let jsonString = String(data: jsonData, encoding: .utf8)
            UserDefaults.standard.set(jsonString, forKey: C.storageKey)
        } catch {
            print("Error occurred persisting photos: error => \(error)")
        }
    }
}
