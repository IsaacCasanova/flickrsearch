//
//  FlickrNetworkManager.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/29/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import UIKit

private struct Constants {
    static let errorDomain = "FlickrSearchErrorDomain"
    static let errorUserInfo = [NSLocalizedDescriptionKey: "Invalid API Response"]
}

private typealias C = Constants

class FlickrNetworkManager {
    
    static let shared = {
        return FlickrNetworkManager()
    }()
    
    private init() { }
    
    func searchForPhoto(query: String,
                        page: String = "1",
                        completionHandler: @escaping (_ results: FlickrSearchResults?, _ error: NSError?) -> Void) {
        
        guard let URL = Flickr.buildSearchURLForSearchTerm(query, page: page) else {
            reportError(nil, completionHandler)
            return
        }
        
        URLSession.shared.dataTask(with: URL) { [unowned self] (data, response, error) in
            
            if let error = error {
                print("Error occurred fetching photos: error => \(error)")
                self.reportError(error as NSError, completionHandler)
            } else if let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = data, !data.isEmpty {
                
                do {
                    
                    let decoded = try JSONDecoder().decode(FlickrSearchResults.self,
                                                           from: data)
                    
                    if decoded.stat != "ok" {
                        self.reportError(nil, completionHandler)
                        return
                    }
                    
                    var flickrPhotos = decoded.photos.photo
                    
                    for i in 0..<flickrPhotos.count {
                        let flickrPhoto = flickrPhotos[i]
                        if let url = flickrPhoto.buildURLForPhoto(),
                            let data = try? Data(contentsOf: url),
                            let image = UIImage(data: data) {
                            flickrPhoto.thumbnail = image
                        }
                    }
                    
                    DispatchQueue.main.async {
                        completionHandler(decoded, nil)
                    }
                    
                } catch {
                    print("decoding: \(String(describing: String(data: data, encoding: .utf8)))")
                    print("Error occurred decoding response: \(error)")
                    self.reportError(nil, completionHandler)
                }
            } else {
                self.reportError(nil, completionHandler)
            }
            }.resume()
    }
    
    func fetchLargeImage(_ photo: FlickrPhoto,
                         completion: @escaping (_ data: UIImage?, _ error: NSError?) -> Void) {
        
        guard let largeURL = photo.buildURLForPhoto("b") else {
            DispatchQueue.main.async {
                completion(nil, nil)
            }
            return
        }
        
        URLSession.shared.dataTask(with: largeURL) { data, response, error in
            
            if let error = error {
                DispatchQueue.main.async {
                    completion(nil, error as NSError)
                }
                return
            }
            
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, nil)
                }
                return
            }
            
            let largeImage = UIImage(data: data)
            DispatchQueue.main.async {
                completion(largeImage, nil)
            }
        }.resume()
        
    }
    
    private func reportError(_ error: NSError? = nil, _
        completionHandler: @escaping (_ results: FlickrSearchResults?, _ error: NSError?) -> Void) {
        let flickrAPIError = NSError(domain: error?.domain ?? C.errorDomain,
                                     code: error?.code ?? 0,
                                     userInfo: error?.userInfo ?? C.errorUserInfo)
        DispatchQueue.main.async {
            completionHandler(nil, flickrAPIError)
            return
        }
    }
}
