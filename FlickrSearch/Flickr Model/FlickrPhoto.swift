//
//  FlickrPhoto.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/29/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import UIKit

class FlickrPhoto: Codable {
    
    let id: String
    let secret: String
    let server: String
    let title: String
    let farm: Int
    
    // not deserialized
    var thumbnail : UIImage? = nil
    var largeImage: UIImage? = nil
    
    private enum CodingKeys: String, CodingKey {
        case id
        case secret
        case server
        case farm
        case title
        case thumbnailData
        case largeImageData
    }
    
    init(id: String,
         secret: String,
         server: String,
         title: String,
         farm: Int) {
        
        self.id = id
        self.secret = secret
        self.server = server
        self.title = title
        self.farm = farm
    }
    
    func buildURLForPhoto(_ size: String = "s") -> URL? {
        
        guard let url = URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_\(size).jpg") else {
            return nil
        }
        
        return url
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        secret = try container.decode(String.self, forKey: .secret)
        server = try container.decode(String.self, forKey: .server)
        title = try container.decode(String.self, forKey: .title)
        farm = try container.decode(Int.self, forKey: .farm)
        
        if let imageData = try container.decodeIfPresent(Data.self, forKey: .thumbnailData) {
            thumbnail = NSKeyedUnarchiver.unarchiveObject(with: imageData) as? UIImage
        }
        
        if let largeImageData = try container.decodeIfPresent(Data.self, forKey: .largeImageData) {
            largeImage = NSKeyedUnarchiver.unarchiveObject(with: largeImageData) as? UIImage
        }
    }
    
    func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(secret, forKey: .secret)
        try container.encode(server, forKey: .server)
        try container.encode(farm, forKey: .farm)
        try container.encode(title, forKey: .title)
        
        if let thumbnail = thumbnail {
            let imageData = NSKeyedArchiver.archivedData(withRootObject: thumbnail)
            try container.encode(imageData, forKey: .thumbnailData)
        }
        
        if let largeImage = largeImage {
            let imageData = NSKeyedArchiver.archivedData(withRootObject: largeImage)
            try container.encode(imageData, forKey: .largeImageData)
        }
    }
}

extension FlickrPhoto: CustomStringConvertible {
    
    var description: String {
        return """
        FlickrPhoto:{id=\(id), secret=\(secret), server=\(server), title=\(title), farm=\(farm)}
        """
    }
}
