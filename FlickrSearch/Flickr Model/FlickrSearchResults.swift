//
//  FlickrSearchResults.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/29/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import Foundation

struct FlickrSearchResults: Codable {
    
    let photos: FlickrPhotoPages
    let stat: String
    
    init(photos: FlickrPhotoPages, stat: String) {
        self.photos = photos
        self.stat = stat
    }
}

extension FlickrSearchResults: CustomStringConvertible {
    
    var description: String {
        return """
        FlickrSearchResults:{ photos=\(photos), stat=\(stat) }
        """
    }
}

struct FlickrPhotoPages: Codable {
    
    let page: Int
    let pages: Int
    let perpage: Int
    let total: String
    let photo: [FlickrPhoto]

    init(page: Int,
         pages: Int,
         perpage: Int,
         total: String,
         photo: [FlickrPhoto]) {
        
        self.page = page
        self.pages = pages
        self.perpage = perpage
        self.total = total
        self.photo = photo
    }
}

extension FlickrPhotoPages: CustomStringConvertible {
    
    var description: String {
        return """
        FlickrPhotoPages:{ page=\(page), pages=\(pages), perpage=\(perpage), total=\(total), photo=\(photo) }
        """
    }
}
