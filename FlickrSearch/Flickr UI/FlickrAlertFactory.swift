//
//  FlickrAlertFactory.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 11/1/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import UIKit

private struct Constants {
    static let alertTitle = "Network Error"
    static let alertMessage = "An error has occurred, please try again later."
    static let dismissActionTitle = "Dismiss"
}

private typealias C = Constants

class FlickrAlertFactory {
    
    class func createErrorAlert(completion: (() -> Void)?) -> UIAlertController {
        
        let alert = UIAlertController(title: C.alertTitle,
                                      message: C.alertMessage,
                                      preferredStyle: .alert)
        
        let action = UIAlertAction(title: C.dismissActionTitle,
                                   style: .default) { _ in
                                    completion?()
        }
        
        alert.addAction(action)
        
        return alert
    }
    
}
