//
//  FlickrDetailPhotoViewController.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/30/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import UIKit

class FlickrDetailPhotoViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: FlickrViewModelDetailType! {
        didSet {
            viewModel.viewDelegate = self
        }
    }
    
    private var imageView = UIImageView()
    
    private var image: UIImage? {
        get { return imageView.image }
        set {
            imageView.image = newValue
            imageView.sizeToFit()
            scrollView?.contentSize = imageView.frame.size
            autoScale()
        }
    }
    
    // MARK: - View Controller Lifecycle Methods
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        autoScale()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.addSubview(imageView)
        viewModel.requestLargeImage()
    }
    
    // MARK: - Helper Methods
    
    private func autoScale() {
        
        if imageView.image == nil { return }
        
        let scaleWidth = scrollView.bounds.size.width/imageView.image!.size.width
        let scaleHeight = scrollView.bounds.size.height/imageView.image!.size.height
        
        let minScale = min(scaleWidth, scaleHeight)
        let maxScale = max(scaleWidth, scaleHeight)
        
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = maxScale
        scrollView.zoomScale = maxScale
        
        let offsetX = (imageView.frame.size.width - scrollView.frame.size.width)/2
        let offsetY = (imageView.frame.size.height - scrollView.frame.size.height)/2
        
        scrollView.contentOffset = CGPoint(x: offsetX, y: offsetY)
    }
}

extension FlickrDetailPhotoViewController: UIScrollViewDelegate {
    
    // MARK: - UIScrollViewDelegate Methods
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}

extension FlickrDetailPhotoViewController: FlickrViewModelDetailViewDelegate {
    
    func dismissSpinner() {
        guard activityIndicator.isAnimating else { return }
        activityIndicator.stopAnimating()
    }
    
    func imageFetchDidFinish(_ image: UIImage) {
        self.image = image
    }
    
    func imageFetchDidFail() {
        
        let alert = FlickrAlertFactory.createErrorAlert { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.navigationController?.popViewController(animated: true)
        }
        
        present(alert, animated: true, completion: nil)
    }
}
