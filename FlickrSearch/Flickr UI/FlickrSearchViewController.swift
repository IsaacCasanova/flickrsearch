//
//  FlickrSearchViewController.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/29/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import UIKit

class FlickrSearchViewController: UITableViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var refreshSpinner: UIRefreshControl!
    
    private lazy var viewModel: FlickrViewModelType = {
       let viewModel = FlickrViewModel()
       viewModel.viewDelegate = self
       return viewModel
    }()
    
    private var cellHeights = [IndexPath: CGFloat]()
    private var firstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshSpinner.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView?.isHidden = true
        viewModel.checkNetworkConnection()
    }
    
    @objc private func refreshData() {
        viewModel.searchText = searchBar.text
        viewModel.performSearchQuery()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? FlickrDetailPhotoViewController,
            let sender = sender as? FlickrPhoto {
            destination.viewModel = FlickrDetailViewModel(photo: sender)
        }
    }
}

// MARK: - UITableViewDataSource

extension FlickrSearchViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return viewModel.photos?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        guard let photos = viewModel.photos else { fatalError("photos array was empty!") }
        
        let photo = photos[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: FlickrTableViewCell.reuseIdentifier,
                                                 for: indexPath) as! FlickrTableViewCell
        
         cell.configureCell(photo: photo)
        
        viewModel.tryFetchNextPage(from: indexPath)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension FlickrSearchViewController {
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
        
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    override func tableView(_ tableView: UITableView,
                            estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let height = cellHeights[indexPath] {
            return height
        }
        
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {
        
        guard let photos = viewModel.photos else { return }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let flickrPhoto = photos[indexPath.row]
        performSegue(withIdentifier: "FlickrPhotoDetailSegue", sender: flickrPhoto)
    }
}

// MARK: - UISearchBarDelegate

extension FlickrSearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        viewModel.searchText = searchBar.text
        viewModel.performSearchQuery()
    }
}

// MARK: - FlickrViewModelViewDelegate

extension FlickrSearchViewController: FlickrViewModelViewDelegate {
    
    func displaySpinner() {
        guard let refreshControl = refreshControl,
        !refreshControl.isRefreshing else { return }
        refreshControl.beginRefreshing()
        
        guard !firstLoad else {
            firstLoad = false
            return
        }
        
        tableView.setContentOffset(CGPoint(x:0, y:tableView.contentOffset.y-refreshControl.frame.size.height), animated:true)
    }
    
    func dismissSpinner() {
        guard let refreshControl = refreshControl,
        refreshControl.isRefreshing else { return }
        refreshControl.endRefreshing()
    }
    
    func displayFooter() {
        guard let tableFooterView = tableView.tableFooterView,
            tableFooterView.isHidden else { return }
        tableFooterView.isHidden = false
    }
    
    func dismissFooter() {
        guard let tableFooterView = tableView.tableFooterView,
            !tableFooterView.isHidden else { return }
        tableFooterView.isHidden = true
    }
    
    func clearCellHeights() {
        cellHeights.removeAll()
    }
    
    func searchQueryDidFinish() {
        tableView.reloadSections(IndexSet(integer: 0), with: .fade)
    }
    
    func pageIncrementDidFinish() {
        tableView.reloadData()
    }
    
    func searchQueryFailed(with error: NSError) {
        print("error: => \(error)")
        let alert = FlickrAlertFactory.createErrorAlert(completion: nil)
        present(alert, animated: true, completion: nil)
    }
    
    func networkUnavailable(_ shouldRefresh: Bool) {
        
        let alert = FlickrAlertFactory.createErrorAlert { [weak self] in
            
            guard let strongSelf = self else { return }
            
            if shouldRefresh {
                strongSelf.tableView.reloadData()
            }
        }
        
        present(alert, animated: true, completion: nil)
    }
}
