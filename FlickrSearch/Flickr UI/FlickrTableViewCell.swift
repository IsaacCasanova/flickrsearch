//
//  FlickrTableViewCell.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/29/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

class FlickrTableViewCell: UITableViewCell {

    @IBOutlet weak var flickrPhotoImage: UIImageView!
    @IBOutlet weak var flickrPhotoTitle: UILabel!
    
    func configureCell(photo: FlickrPhoto) {
        flickrPhotoImage.image = photo.thumbnail
        flickrPhotoTitle.text = photo.title
    }
}
