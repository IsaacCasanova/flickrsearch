//
//  File.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 11/1/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import UIKit.UIImage

protocol FlickrViewModelDetailType {
    init(photo: FlickrPhoto)
    var viewDelegate: FlickrViewModelDetailViewDelegate? { get set }
    func requestLargeImage()
}

protocol FlickrViewModelDetailViewDelegate: class {
    func dismissSpinner()
    func imageFetchDidFinish(_ image: UIImage)
    func imageFetchDidFail()
}

class FlickrDetailViewModel: FlickrViewModelDetailType {
    
    var viewDelegate: FlickrViewModelDetailViewDelegate?
    private let photo: FlickrPhoto
    
    required init(photo: FlickrPhoto) {
        self.photo = photo
    }
    
    func requestLargeImage() {
        
        FlickrNetworkManager.shared.fetchLargeImage(photo) { [weak self] (image, error) in
            
            guard let strongSelf = self else { return }
            
            strongSelf.viewDelegate?.dismissSpinner()
            
            guard error == nil else {
                strongSelf.viewDelegate?.imageFetchDidFail()
                return
            }
            
            guard let image = image else {
                strongSelf.viewDelegate?.imageFetchDidFail()
                return
            }
        
            strongSelf.viewDelegate?.imageFetchDidFinish(image)
        }
    }
}
