//
//  FlickrViewModel.swift
//  FlickrSearch
//
//  Created by Isaac  Casanova on 10/31/17.
//  Copyright © 2017 Isaac  Casanova. All rights reserved.
//

import Foundation

protocol FlickrViewModelViewDelegate: class {
    func pageIncrementDidFinish()
    func searchQueryDidFinish()
    func searchQueryFailed(with error: NSError)
    func networkUnavailable(_ shouldRefresh: Bool)
    func displayFooter()
    func displaySpinner()
    func dismissSpinner()
    func dismissFooter()
    func clearCellHeights()
}

protocol FlickrViewModelType {
    
    var viewDelegate: FlickrViewModelViewDelegate? { get set }
    var searchText: String? { get set }
    var photos: [FlickrPhoto]? { get set }
    var textChanged: Bool { get set }
    var maximumItems: Int { get }
    var isLoading: Bool { get set }
    var numberOfPages: Int? { get set }
    
    func performSearchQuery()
    func checkNetworkConnection()
    func tryFetchNextPage(from indexPath: IndexPath)
}

// MARK: - FlickrViewModelType Protocol

class FlickrViewModel: FlickrViewModelType {
    
    weak var viewDelegate: FlickrViewModelViewDelegate?
    
    var searchText: String? {
        willSet {
            if let newValue = newValue,
                newValue != searchText {
                textChanged = true
            }
        }
    }
    
    var photos: [FlickrPhoto]? {
        didSet {
            if let photos = photos, !photos.isEmpty {
                FlickrDataStore.shared.persistPhotos(photos)
            }
        }
    }
    
    var maximumItems = 200
    var textChanged = false
    var isLoading = false
    var numberOfPages: Int? = nil
    
    func checkNetworkConnection() {
        
        FlickrNetworkManager.shared.searchForPhoto(query: "networkCheck") { [weak self] (photos, error) in
            
            guard let strongSelf = self else { return }
            
            if strongSelf.noInternetConnectionFound(error) {
                strongSelf.readStoredPhotos()
                return
            }
        }
    }

    func performSearchQuery() {
        
        viewDelegate?.displaySpinner()
        
        guard !isLoading,
            let searchText = searchText,
            !searchText.isEmpty,
            textChanged || (photos?.count ?? 0) < maximumItems else {
                viewDelegate?.dismissSpinner()
                return
        }
        
        isLoading = true
        let page = calculatePage()
        
        if let numberOfPages = numberOfPages,
            Int(page)! > numberOfPages {
            viewDelegate?.dismissSpinner()
            return
        }
        
        FlickrNetworkManager.shared.searchForPhoto(query: searchText, page: page) { [weak self] (results, error)  in
            
            guard let strongSelf = self else { return }
            strongSelf.isLoading = false
            
            strongSelf.viewDelegate?.dismissFooter()
            
            guard !strongSelf.noInternetConnectionFound(error) else {
                strongSelf.viewDelegate?.dismissSpinner()
                strongSelf.readStoredPhotos()
                return
            }
            
            guard error == nil else {
                strongSelf.viewDelegate?.dismissSpinner()
                strongSelf.viewDelegate?.searchQueryFailed(with: error! as NSError)
                return
            }
            
            guard let photos = results?.photos.photo,
                !photos.isEmpty else {
                    strongSelf.viewDelegate?.dismissSpinner()
                    return
            }
            
            strongSelf.numberOfPages = results!.photos.pages
            
            guard !strongSelf.textChanged else {
                strongSelf.photos = photos
                strongSelf.textChanged = false
                strongSelf.viewDelegate?.clearCellHeights()
                strongSelf.viewDelegate?.dismissSpinner()
                strongSelf.viewDelegate?.searchQueryDidFinish()
                return
            }
            
            guard var strongPhotos = strongSelf.photos else {
                strongSelf.photos = photos
                strongSelf.viewDelegate?.dismissSpinner()
                strongSelf.viewDelegate?.searchQueryDidFinish()
                return
            }
            
            strongPhotos.append(contentsOf: photos)
            strongSelf.photos = strongPhotos
            strongSelf.viewDelegate?.dismissSpinner()
            strongSelf.viewDelegate?.pageIncrementDidFinish()
        }
    }
    
    func tryFetchNextPage(from indexPath: IndexPath) {
        
        guard let photos = photos,
            let numberOfPages = numberOfPages,
            Int(calculatePage())! <= numberOfPages,
            photos.count < maximumItems,
            indexPath.row == photos.count - 1 else {
                return
        }
        
        viewDelegate?.displayFooter()
        performSearchQuery()
    }
}

// MARK: - Helpers

extension FlickrViewModel {
    
    private func noInternetConnectionFound(_ error: NSError?) -> Bool {
        if let error = error,
            error.code == NSURLErrorNotConnectedToInternet {
            return true
        }
        return false
    }
    
    private func readStoredPhotos() {
        
        var storedPhotos: [FlickrPhoto]? = nil
        
        if photos == nil || photos!.isEmpty {
            storedPhotos = FlickrDataStore.shared.readPhotos()
            photos = storedPhotos
        }
        
        let shouldRefresh = photos != nil && !photos!.isEmpty
        viewDelegate?.networkUnavailable(shouldRefresh)
    }
    
    private func calculatePage() -> String {
        
        guard let photos = photos,
            !photos.isEmpty else { return "1" }
        
        let page = (photos.count / Int(perPage)!) + 1
        return "\(page)"
    }
}
